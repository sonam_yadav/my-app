var passport = require('passport'),
    mongoose = require('mongoose');

require('../app/models/user.server.model.js')
module.exports = function() {
    var User = mongoose.model('users');

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findOne(
            {_id: id},
            '-password',
            function(err, user) {
                done(err, user);
            }
        );
    });

    require('./strategies/local.js')();
};