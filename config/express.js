/**
 * Created by comtech on 16/4/16.
 */
var config = require('./config'),
    express = require('express'),
    bodyParser = require('body-parser');
passport = require('passport');

var flash = require('connect-flash');

module.exports = function() {
    var app = express();

    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(bodyParser.json());

    app.set('views', './public/views');
    //app.set('view engine', 'ejs');
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    app.use(flash());
    var session = require('express-session');

    app.use(session({
        saveUninitialized: true,
        resave: true,
        secret: 'OurSuperSecretCookieSecret'
    }));
//use this code before any route definitions
    app.use(passport.initialize());
    app.use(passport.session());

    require('../app/routes/index.server.routes.js')(app);
    require('../app/routes/users.server.routes.js')(app);
    require('../app/routes/entity.server.routes.js')(app);

    app.use(express.static('./public'));

    return app;
};
