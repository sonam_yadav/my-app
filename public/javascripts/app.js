/**
 * Created by comtech on 16/4/16.
 */

// app.js
var schoolApp = angular.module('school', ['ui.router','school.entity','ajoslin.promise-tracker']);
angular.module('school.entity', []);
angular.module('ajoslin.promise-tracker',[]);

schoolApp.config(function($stateProvider, $urlRouterProvider) {

    //================================================
    // Check if the user is connected
    //================================================
    var checkLoggedin = function($q, $timeout, $http, $location, $rootScope){
        // Initialize a new promise
        var deferred = $q.defer();
        console.log('here in check=====')
        // Make an AJAX call to check if the user is logged in
        $http.get('/api/v1/checkuser').then(function mySuccess(response){
            // Authenticated
            console.log('in check config user====',response)
            if (response.data){
                console.log('here in  user000000====',response);
                /*$timeout(deferred.resolve, 0);*/
                deferred.resolve();
            }
            else {
                console.log('here in else user====',response);

            }
        },function myError(response){
            console.log('err0rrrr=====',response)
            $rootScope.message = 'You need to log in.';
            $timeout(function(){deferred.reject();}, 0);
            //deferred.reject();
            $location.url('/');
        })

        return deferred.promise;
    };
    //================================================


    $urlRouterProvider.otherwise('/');

    $stateProvider
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/',
            templateUrl: '../views/home.html'
        })
        .state('login', {
            url: '/login',
            templateUrl: '../views/login.html'
        })
        .state('register', {
            url: '/register',
            templateUrl: '../views/register.html'
        })
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: '../views/dashboard.html',
            resolve: {
                loggedin: checkLoggedin
            }
        })
        .state('get', {
            url: '/get/:entity/:id',
            templateUrl: '../views/get.html',
            resolve: {
                loggedin: checkLoggedin
            }

        })
        .state('edit', {
            url: '/edit/:entity/:id',
            templateUrl: '../views/add.html',
            resolve: {
                loggedin: checkLoggedin
            }
        })
        .state('list', {
            url: '/list/:entity',
            templateUrl: '../views/list.html',
            resolve: {
                loggedin: checkLoggedin
            }
        })
        .state('add', {
            url: '/add/:entity',
            templateUrl: '../views/add.html',
            resolve: {
                loggedin: checkLoggedin
            }
        })
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            // we'll get to this in a bit
        });

});
