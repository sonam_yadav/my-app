/**
 * Created by comtech on 16/4/16.
 */
//Job Application factory service used for REST endpoint
angular.module('school.entity').factory('Entity', ['$http', '$q', function($http, $q) {

    var urlBase = '/api/v1/entity';
    var Entity = {};

    Entity.getAll = function(entityname) {
        return $http.get(urlBase+'/'+entityname);
    };
    Entity.add = function(entityname,data) {
        return $http.post(urlBase+'/'+entityname,{data:data});
    };
    Entity.getById = function(entityname,id) {
        return $http.get(urlBase+'/'+entityname+'/'+id);
    };
    Entity.checkUser = function(entityname,id) {
        return $http.get('/api/v1/checkuser');
    };

    return Entity;

}]);
