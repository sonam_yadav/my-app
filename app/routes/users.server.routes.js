var users = require('../../app/controllers/users.server.controller');

module.exports = function(app) {

    app.route('/register')
        //.get(users.renderRegister)
        .post(users.register);

    app.route('/login')
        //.get(users.renderLogin)
        .post(passport.authenticate('local', {
            successRedirect: '/#/dashboard',
            failureRedirect: '/#/login',
            failureFlash: true
        }));

    app.get('/logout', users.logout);
    app.get('/api/v1/checkuser', users.checkuser);
    app.route('/users').post(users.create).get(users.list);

   // app.route('/users/:userId').get(users.read).put(users.update);
    app.route('/users/:userId').get(users.read).put(users.update).delete(users.delete);

    app.param('userId', users.userByID);
};