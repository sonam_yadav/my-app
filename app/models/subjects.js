
/**
 * Created by comtech on 16/4/16.
 */
var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

var StubjectSchema = new Schema({
    name: {type:String,unique : true, required : true},
    students:[ {
        type: Schema.ObjectId,
        ref: 'students'}],
    teacher: {
        type: Schema.ObjectId,
        ref: 'teachers'
    },
    type:String
});

mongoose.model('subjects', StubjectSchema);