
/**
 * Created by comtech on 16/4/16.
 */
var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

var StudentSchema = new Schema({
    name: {type:String,unique : true, required : true},
    subjects:[ {
        type: Schema.ObjectId,
        ref: 'subjects'}],
    course:String
});

mongoose.model('students', StudentSchema);