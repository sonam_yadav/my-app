/**
 * Created by comtech on 16/4/16.
 */


angular.module('school.entity').controller('entityrecordsctrl', ['$scope','$stateParams','$http','Entity', function ($scope,$stateParams,$http,Entity) {
    $scope.entitydata={},$scope.entitydata.subjects=[];
    $scope.entityname=$stateParams.entity;
    $scope.selection=[];
    $scope.getseeddata=function(){
        console.log('get all seed',$stateParams);
        var entityname=$stateParams.entity;
        Entity.getAll('subjects').success(function(data, status) {
            $scope.subjects = data;
            console.log('subjects frontend=====',$scope.subjects)
            Entity.getAll('teachers').success(function(data, status) {
                $scope.teachers = data;
                console.log('teachers frontend=====',$scope.teachers)
            })
                .error(function(err, status) {
                    $scope.errorMessage = "Error retrieving records.";
                });
        })
            .error(function(err, status) {
                $scope.errorMessage = "Error retrieving records.";
            });
    }

    $scope.toggleSelection = function toggleSelection(item) {
        var idx = $scope.entitydata.subjects.indexOf(item);

        // is currently selected
        if (idx > -1) {
            $scope.entitydata.subjects.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.entitydata.subjects.push(item);
        }
    };

    $scope.getAll=function(){
        console.log('get all',$stateParams);
        var entityname=$stateParams.entity;
        Entity.getAll(entityname).success(function(data, status) {
            $scope.data = data;
            console.log('data frontend=====',$scope.data)
        })
            .error(function(err, status) {
                $scope.errorMessage = "Error retrieving records.";
            });
    }

    $scope.add=function(){
        console.log('add',$stateParams, $scope.entitydata);
        var entityname=$stateParams.entity;
        var data;
        if(entityname=='candidates'){
            $scope.entitydata.subjects=[];
            $scope.entitydata.subjects=$scope.selection;
        }
        data=$scope.entitydata;

console.log('data======',data,$scope.selection,$scope.entitydata);
        Entity.add(entityname,data).success(function(data, status) {
            $scope.data = data;
            console.log('data after save=====',$scope.data)
        })
            .error(function(err, status) {
                $scope.errorMessage = "Error retrieving records.";
            });
    }

    $scope.getRecord=function(){
        console.log('add',$stateParams, $scope.entitydata);
        var entityname=$stateParams.entity;
        var id=$stateParams.id;

        Entity.getById(entityname,id).success(function(data, status) {
            $scope.data = data;
            console.log('data after save=====',data,status)
        })
            .error(function(err, status) {
                $scope.errorMessage = "Error retrieving records.";
            });
    }
}]);