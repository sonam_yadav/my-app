/**
 * Created by comtech on 16/4/16.
 */
var passport = require('passport');


exports.list = function(req, res, next) {
    var entityname=req.params.entityName;
    console.log('user====',JSON.stringify(req.user))
    var col = require('mongoose').model(entityname);
    var populate;
    if(entityname=='students'){
        populate='teacher subjects'
    }else
    if(entityname=='teachers'){
        populate='subject'

    }else
    if(entityname=='subjects'){
        populate='teacher students'

    }
    col.find({}).populate(populate).exec(function(err, records) {
        if (err) {
            return next(err);
        }
        else {
            res.json(records);
        }
    });
};
var execpostHook=function(name,model,next){
    if(name=='students'){
        if(model.subjects){
            var refModel=require('mongoose').model('subjects')
            //refModel=new refModel();
            var c=model.subjects.length;
            model.subjects.forEach(function(item){
                refModel.update({_id:item},{$push:{students:model._id}},function(err,result){
                    console.log('after post hook=====',err,result)
                    c--;
                    if(c==0){
                        if(model.teachers){
                            var refModel2=require('mongoose').model('teachers')
                            var c=model.teachers.length;
                            model.teachers.forEach(function(item){
                                refModel2.update({_id:item},{$push:{students:model._id}},function(err,result){
                                    console.log('after post hook=====',err,result)
                                    c--;
                                    if(c==0){
                                        if(model.teachers){

                                        }
                                    }
                                })
                            })
                        }
                    }
                })
            })
        }
    }
};
exports.create = function(req, res, next) {
    var entityname=req.params.entityName;
    console.log('req==============',req.body);
    //return;
    var execHook=false;
    if(entityname=='students'){
        execHook=true;
    }
    var Model = require('mongoose').model(entityname);
    Model = new Model(req.body.data);
    Model.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            if(execHook){
                execpostHook(entityname,Model,function(){
                    res.json(Model);
                })
            }
            else{
                res.json(Model);
            }
        }
    });
};

exports.get = function(req, res, next) {
    var entityname=req.params.entityName;
    var id=req.params.id;
    //return;
    var populate;
    if(entityname=='students'){
        populate='subjects subjects.teacher'
    }else
    if(entityname=='teachers'){
        populate='students subject'

    }else
    if(entityname=='subjects'){
        populate='students teacher'

    }
    var data=[]
    function getrefField(cloned,subfield,next){
        var col1 = require('mongoose').model('teachers');

        if(cloned.length==0){
           return next(data);
        }
        var item=cloned.shift()

        col1.findOne({_id:item[subfield]},function(err,teacher){
            console.log('here innnnnnnn=====',JSON.stringify(item),JSON.stringify(teacher))

            if (err) {
                return next(err);
            }
            else {
                item[subfield]=teacher;
                data.push(item);
                console.log('d1111111====',data)
                getrefField(cloned,subfield,next)
            }
        })

    }
    var Model = require('mongoose').model(entityname);
    Model.findOne({_id:id}).populate(populate).exec(function(err,result) {
        console.log('get record======',entityname,JSON.stringify(result))
        if(entityname=='students'){
           getrefField(result.subjects,'teacher',function(data){
               console.log('data after process=====',JSON.stringify(data))
               result.subjects=data;
               res.json(result);
           })

        }else
        {
            if (err) {
                return next(err);
            }
            else {
                res.json(result);
            }
        }

    });
};
