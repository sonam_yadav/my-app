/**
 * Created by comtech on 17/4/16.
 */

var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

var TeacherSchema = new Schema({
    name: {type:String,unique : true, required : true},
    students:[ {
        type: Schema.ObjectId,
        ref: 'students'}],
    subject: {
        type: Schema.ObjectId,
        ref: 'subjects'
    },
    deptt:String
});

mongoose.model('teachers', TeacherSchema);