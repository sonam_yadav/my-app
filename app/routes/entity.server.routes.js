/**
 * Created by comtech on 16/4/16.
 */
var entity = require('../../app/controllers/entity.server.controller');

module.exports = function(app) {

    app.route('/api/v1/entity/:entityName').get(entity.list).post(entity.create);
    app.route('/api/v1/entity/:entityName/:id').get(entity.get);

    //app.param('userId', users.userByID);
};