/**
 * Created by comtech on 16/4/16.
 */
//Setting up route
angular.module('school').config(['$routeProvider',
    function($routeProvider) {

        //================================================
        // Check if the user is connected
        //================================================
        var checkLoggedin = function($q, $timeout, $http, $location, $rootScope){
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            if ($rootScope.user){
                console.log('here  user000000====',user);
                /*$timeout(deferred.resolve, 0);*/
                deferred.resolve();
            }
            // Not Authenticated
            else {
                console.log('here in else user====',user);
                $rootScope.message = 'You need to log in.';
                //$timeout(function(){deferred.reject();}, 0);
                deferred.reject();
                $location.url('/');
            }

            return deferred.promise;
        };
        //================================================

        $routeProvider.
            when('/dashboard', {
                templateUrl: 'views/dashboard/commondash.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            }).
            otherwise({
                redirectTo: '/'
            });
    }
]);

//Setting HTML5 Location Mode
angular.module('mean').config(['$locationProvider',
    function($locationProvider) {
        $locationProvider.hashPrefix("!");
    }
]);



// Setting up Interceptors
angular.module('mean').config(['$httpProvider',
    function($httpProvider) {
        // http interceptors for Unauthorized access
        $httpProvider.interceptors.push(['$q', '$location', function($q, $location) {

            return {
                response: function(response) {
                    // response.status === 200
                    return response || $q.when(response);
                },
                responseError: function(rejection) {
                    // Executed only when the XHR response has an error status code

                    if (rejection.status == 401) {

                        // The interceptor "blocks" the error;
                        // and the success callback will be executed.

                        rejection.data = { status: 401, description: 'unauthorized' };
                        console.log("Rejection data: " + JSON.stringify(rejection.data));
                        //$location.path("/");
                    }

                    /*
                     $q.reject creates a promise that is resolved as
                     rejected with the specified reason.
                     In this case the error callback will be executed.
                     */

                    return $q.reject(rejection);
                }
            };

        }]);
    }
]);
